import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'
import {
  Button,
  Input,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Form,
} from 'element-ui'

Vue.use(Input)
Vue.use(Button)
Vue.use(Checkbox)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)
Vue.use(Form)
