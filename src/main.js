import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/plugins/element'
import '@/assets/style/app.scss'
import vuetify from './plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@/plugins/vuemask'

Vue.config.productionTip = false

Vue.config.productionTip = false

const app = new Vue({
  router,
  vuetify,
  render: h => h(App),
})

app.$mount('#app')
