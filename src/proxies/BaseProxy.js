import qs from 'qs'
import { MEDITOPIA } from '../utils/request'

const datableTypes = ['post', 'put']

const ApiMap = new Map()
ApiMap.set('meditopia', MEDITOPIA)

export class BaseProxy {
  constructor ({ api = 'meditopia', endpoint, parameters = { headers: {} } }) {
    this.$http = MEDITOPIA
    this.endpoint = endpoint
    this.parameters = parameters
    this.config = {}
  }

  submit (requestType, url, data = null) {
    return new Promise((resolve, reject) => {
      const args = [url + this.getParameterString(), this.config]
      if (datableTypes.includes(requestType)) {
        args.splice(1, 0, data)
      }
      this.$http[requestType](...args)
        .then((response) => {
          resolve(response.data)
        })
        .catch(({ response }) => {
          if (response) {
            reject(response.data)
          } else {
            reject(response)
          }
        })
    })
  }

  all () {
    return this.submit('get', `/${this.endpoint}`)
  }

  getParameterString () {
    const parameterStrings = qs.stringify(this.parameters, { encodeValuesOnly: true })

    return parameterStrings.length === 0 ? '' : `?${parameterStrings}`
  }
}
