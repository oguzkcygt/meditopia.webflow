import Axios from 'axios'

const MEDITOPIA = Axios.create({
  baseURL: 'https://api.meditopia.com/',
  headers: {},
})

Axios.defaults.params = {}

export {
  MEDITOPIA,
}
